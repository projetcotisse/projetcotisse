DROP TABLE IF EXISTS Utilisateurs
DROP TABLE IF EXISTS Profil;

CREATE TABLE Profil (
  id VARCHAR(10) primary key,
  nom VARCHAR(50)
);
INSERT INTO Profil values ('PRF-1','Admin');
CREATE TABLE Utilisateurs(
  id VARCHAR(10) primary key,
  nom varchar(255) default NULL,
  prenom varchar(255) default NULL,
  sexe varchar(255) default NULL,
  dateDeNaissance DATE,
  telephone varchar(100) default NULL,
  email varchar(255) default NULL,
  motDePasse varchar(255),
  idProfil varchar(10) REFERENCES Profil(id)
);
CREATE SEQUENCE idutilisateurs START 5;

CREATE TABLE Category(
  id VARCHAR(10) primary key,
  category VARCHAR(50),
  pourcentage FLOAT
);
insert into Category (id,category,pourcentage) values ('CAT-1','LITE',0.1) , ('CAT-2','PREMIUM',0.3) , ('CAT-3','VIP',0.5);

CREATE TABLE Vehicule(
  immatriculation VARCHAR(10) primary key,
  place integer,
  idCategory VARCHAR(10) REFERENCES Category(id)
);
insert into Vehicule (immatriculation,place,idCategory) values ('1890 TBA',25,'CAT-1') , ('9836 TBD',9,'CAT-3') , ('2587 TCA',15,'CAT-2');

CREATE TABLE Lieu(
  id VARCHAR(10) primary key,
  lieu VARCHAR(50)
);
insert into Lieu (id,lieu) values ('LI-1','ANTANANARIVO'),('LI-2','MAHAJANGA'),('LI-3','TOAMASINA'),('LI-4','ANTSIRANANA'),('LI-5','TOLIARA');

CREATE TABLE Trajet(
  id VARCHAR(10) primary key,
  depart VARCHAR(10) REFERENCES Lieu(id),
  arrivee VARCHAR(10) REFERENCES Lieu(id),
  heure VARCHAR(50),
  prix FLOAT
);
insert into Trajet (id,depart,arrivee,heure,prix) values ('TR-1','LI-1','LI-2','13h',40000),('TR-2','LI-2','LI-1','22h',40000),('TR-3','LI-1','LI-2','8h',40000),('TR-4','LI-1','LI-3','22h30',30000),('TR-5','LI-3','LI-1','8h',30000);
insert into Trajet (id,depart,arrivee,heure,prix) values ('TR-6','LI-4','LI-5','12h',120000);



CREATE TABLE Place(
  id VARCHAR(10) primary key,
  idVehicule VARCHAR(10) REFERENCES Vehicule(immatriculation),
  numPlace VARCHAR(10)
);
INSERT INTO Place values ('PL-1','1890 TBA','12-13-14'),('PL-2','9836 TBD','2-3');
CREATE TABLE Voyage(
  id VARCHAR(10) primary key,
  idVehicule VARCHAR(10) REFERENCES Vehicule(immatriculation),
  idTrajet VARCHAR(10) REFERENCES Trajet(id)
);
insert into Voyage (id,idVehicule,idTrajet) values ('VG-1','9836 TBD','TR-1'),('VG-2','2587 TCA','TR-1'),('VG-3','1890 TBA','TR-1');
insert into Voyage (id,idVehicule,idTrajet) values ('VG-4','9836 TBD','TR-2'),('VG-5','2587 TCA','TR-2'),('VG-6','1890 TBA','TR-2');
insert into Voyage (id,idVehicule,idTrajet) values ('VG-7','9836 TBD','TR-6'),('VG-8','2587 TCA','TR-6'),('VG-9','1890 TBA','TR-6');

CREATE TABLE Reservation(
  id VARCHAR(10) primary key,
  idVoyage VARCHAR(10) REFERENCES Voyage(id),
  idPlace VARCHAR(10) REFERENCES Place(id),
  iUtilisateurs VARCHAR(10) REFERENCES Utilisateurs(id),
  daty DATE,
  etat INTEGER
);
INSERT INTO Reservation values ('RV-1','VG-1','PL-2','USR-1','23-01-2020',1);

CREATE TABLE Billet(
  id VARCHAR(10) primary key,
  etat INTEGER,
  total FLOAT
);
create SEQUENCE idBillet;
CREATE TABLE BilletDetails(
  id VARCHAR(10) primary key,
  idReservation VARCHAR(10) REFERENCES Reservation(id),
  idBillet VARCHAR(10) REFERENCES Billet(id),
  prix FLOAT
);
create sequence idBilletDetails;
CREATE TABLE Avantages(
  id VARCHAR(10) primary key,
  avantages VARCHAR(50)
);
insert into Avantages (id,avantages) values ('AV-1','Chocolat'),('AV-2','Transport à domicile');

  CREATE TABLE AvantagesPlace(
    idAvantages VARCHAR(10) REFERENCES Avantages(id),
    idPlace VARCHAR(10) REFERENCES Place(id)
  );


create or replace view  voyageAffiche as select Voyage.id as idVoyage,immatriculation,Trajet.id as idTrajet,(SELECT Lieu.lieu from Lieu where Lieu.id = depart) as depart, (SELECT Lieu.lieu from Lieu where Lieu.id = arrivee) as arrivee,heure,Vehicule.place,Vehicule.idCategory,Category.category,Category.pourcentage ,prix , (prix + (prix * Category.pourcentage)) as PRIXTOTAL
  from Voyage 
    join Trajet on Voyage.idTrajet = Trajet.id 
    join Vehicule on Vehicule.immatriculation = immatriculation 
    join Category   on idCategory = Category.id
    join Lieu on Lieu.id = depart;

create view reservationAffiche as select voyageAffiche.*,reservation.id as idReservation ,reservation.iutilisateurs, reservation.daty , reservation.etat as etatReservation , place.id as idPlace , place.numplace 
  from voyageAffiche 
    join reservation on voyageaffiche.idvoyage = reservation.idvoyage 
    join place on place.id = reservation.idplace 
  -- where iutilisateurs = 'USR-1';

  create view vehiculeCategorie as select Vehicule.*, Category.category, Category.pourcentage from Vehicule JOIN Category on Vehicule.idCategory = Category.id;

  create view depart as select Trajet.id,Trajet.heure,Trajet.prix, Lieu.lieu from Trajet JOIN Lieu ON Trajet.depart = Lieu.id;
    create view arriver as select Trajet.id,Trajet.heure,Trajet.prix, Lieu.lieu from Trajet JOIN Lieu ON Trajet.arrivee = Lieu.id;
    create view TrajetLieu as select depart.*, arriver.lieu as lieuArrivee from depart join arriver on depart.id = arriver.id;  

  create view reserver as select Reservation.*, Voyage.idVehicule, Voyage.idTrajet, Place.numPlace 
  from Reservation JOIN Voyage on Reservation.idVoyage = Voyage.id JOIN Place on Reservation.idPlace = Place.id;

INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-1','Mejia','Oren','Feminin','03/03/2010','032 50 456 20','mejia@gmail.com',(SELECT digest('F57U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-2','Morris','Leandra','Feminin','05/07/1965',' 034 06 169 97','leandra@gmail.com',(SELECT digest('M14W','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-3','Austin','Britanni','Masculin','21/12/1970',' 034 54 543 09','austin@gmail.com',(SELECT digest('Z11R','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-4','Burris','Scott','Feminin','19/02/2000',' 033 04 308 86','sapien@nuncsedlibero.org',(SELECT digest('B37E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-5','Tyler','Valentine','Feminin','27/12/2008','032 74 095 21','ante@lectusCum.ca',(SELECT digest('D15X','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-6','Knowles','Cameron','Feminin','13/04/2013','032 69 502 01','pharetra@commodotinciduntnibh.com',(SELECT digest('F25G','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-7','Roberts','Fleur','Feminin','26/06/1990','032 62 607 23','Cras@vestibulumMauris.co.uk',(SELECT digest('Z85J','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-8','Jacobs','Portia','Feminin','30/09/1960','032 63 665 73','Fusce.feugiat.Lorem@tempor.ca',(SELECT digest('Z95H','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-9','Mckenzie','Judith','Feminin','04/12/1989','032 23 265 23','varius@vitaeposuereat.com',(SELECT digest('T85X','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-10','Petersen','Ora','Feminin','14/04/2013',' 033 43 091 33','ipsum.porta@nisi.com',(SELECT digest('M24S','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-11','Donovan','Cole','Feminin','12/10/2003','032 68 658 58','parturient.montes@atlacusQuisque.org',(SELECT digest('I08O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-12','Stephenson','Preston','Feminin','09/03/1970',' 033 99 179 21','enim.consequat.purus@adipiscinglobortis.net',(SELECT digest('Y16M','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-13','Kidd','Lavinia','Feminin','12/10/2004','032 08 283 10','euismod.urna@nuncrisus.org',(SELECT digest('F36A','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-14','Gordon','William','Feminin','27/10/1998',' 034 19 936 74','mauris.a.nunc@est.net',(SELECT digest('J29L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-15','Davidson','Warren','Masculin','01/11/1967','032 59 882 65','magna@auctor.net',(SELECT digest('P23K','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-16','Goff','Emmanuel','Masculin','08/02/2004','032 09 832 18','Vivamus.molestie.dapibus@sapienAenean.co.uk',(SELECT digest('X59Q','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-17','Cohen','Tanner','Feminin','07/04/1968',' 033 81 612 24','ut@malesuadavelvenenatis.edu',(SELECT digest('Y41U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-18','Finley','Tanek','Feminin','30/06/1974',' 033 03 695 38','eu.tellus.eu@nisiCumsociis.edu',(SELECT digest('O64P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-19','Cook','Cameron','Feminin','23/06/1974','032 67 236 64','ligula.consectetuer.rhoncus@turpisAliquam.ca',(SELECT digest('H92D','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-20','Herrera','Jael','Feminin','17/10/2009',' 033 37 420 80','dolor.Donec.fringilla@Nullaeu.co.uk',(SELECT digest('Y47N','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-21','Higgins','Abraham','Feminin','18/03/2013',' 033 52 365 91','Suspendisse.ac.metus@posuerecubiliaCurae.ca',(SELECT digest('G31M','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-22','Klein','Bert','Feminin','29/11/2002',' 034 60 689 17','amet.metus@eutemporerat.co.uk',(SELECT digest('W10N','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-23','Guerrero','Ocean','Feminin','30/04/2004',' 034 80 063 54','neque.venenatis.lacus@felisDonec.edu',(SELECT digest('T60Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-24','Frye','Imelda','Feminin','10/01/1968',' 033 36 200 46','dictum.ultricies.ligula@felis.com',(SELECT digest('Z30O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-25','Cole','Audra','Masculin','31/01/1971',' 034 29 478 98','Donec.porttitor.tellus@Ut.org',(SELECT digest('J27M','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-26','Bell','Seth','Masculin','05/09/1960','032 39 241 91','lorem.luctus.ut@Craseu.edu',(SELECT digest('V90I','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-27','Ross','Arthur','Masculin','27/12/1968','032 52 954 58','nec@Aliquamadipiscing.org',(SELECT digest('C29F','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-28','Watkins','Amelia','Masculin','07/06/1997','032 22 578 17','Donec@miDuis.com',(SELECT digest('N05U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-29','Reynolds','Walter','Masculin','22/08/2001',' 033 37 393 41','tellus@euaccumsan.com',(SELECT digest('S33Q','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-30','Osborne','Cameron','Masculin','08/01/1996','032 61 761 30','ac.libero@Donecnibh.ca',(SELECT digest('M14C','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-31','Shepard','Keith','Feminin','26/11/1967','032 73 611 70','lorem.Donec@purus.org',(SELECT digest('P43G','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-32','Frazier','Mara','Feminin','13/08/1997',' 034 01 668 31','sollicitudin.adipiscing@euturpis.co.uk',(SELECT digest('Z19V','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-33','Kline','Cairo','Masculin','30/08/1961','032 82 144 72','nec.euismod@ullamcorperDuisat.edu',(SELECT digest('O43T','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-34','Holder','Quynn','Feminin','15/07/2012','032 69 782 59','egestas.rhoncus.Proin@Quisqueornaretortor.net',(SELECT digest('S27N','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-35','Glover','Denton','Feminin','13/05/1970',' 034 44 712 20','varius@aliquetlobortisnisi.org',(SELECT digest('P16I','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-36','Acosta','Madison','Feminin','19/03/1987',' 034 47 089 06','aliquet.magna.a@orciquislectus.net',(SELECT digest('V72E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-37','Meyer','Calvin','Masculin','23/10/1961',' 033 20 709 94','Fusce.aliquam.enim@turpisIn.com',(SELECT digest('F28T','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-38','Hendricks','Hamish','Feminin','06/04/1961','032 05 016 51','vitae@sociisnatoque.com',(SELECT digest('O58N','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-39','Armstrong','Sonia','Feminin','03/05/2007','032 66 577 93','mauris@iaculisaliquetdiam.com',(SELECT digest('I92L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-40','Vazquez','Damon','Masculin','24/02/1985',' 034 82 611 50','Morbi.sit@pedeet.ca',(SELECT digest('P18P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-41','Knapp','Olivia','Feminin','22/03/1968',' 033 41 991 03','Nunc.ullamcorper.velit@acmieleifend.edu',(SELECT digest('S72E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-42','Bradshaw','Quincy','Masculin','26/10/1981',' 034 79 177 02','imperdiet.dictum.magna@ornarelectus.ca',(SELECT digest('L50O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-43','Branch','Nayda','Masculin','15/06/1982',' 034 76 265 43','ac@lectussit.org',(SELECT digest('S82O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-44','Deleon','Aquila','Feminin','04/04/1973','032 38 494 30','non@nonjustoProin.ca',(SELECT digest('J71T','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-45','Cline','Kasimir','Masculin','03/02/1975',' 033 82 535 72','elit.Nulla.facilisi@tempusmauriserat.ca',(SELECT digest('A92H','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-46','Fuentes','Valentine','Feminin','25/12/1966','032 27 680 76','ante.ipsum.primis@ultricies.edu',(SELECT digest('T30Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-47','Curtis','Natalie','Masculin','02/08/1989',' 034 10 604 24','Suspendisse.sagittis.Nullam@dictum.co.uk',(SELECT digest('Y30E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-48','West','Wylie','Masculin','05/07/2001','032 24 781 17','sit.amet@vitaerisusDuis.net',(SELECT digest('U46Y','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-49','Horne','Francesca','Masculin','14/01/1991','032 43 339 84','arcu.eu@odio.net',(SELECT digest('L12F','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-50','Lee','Penelope','Feminin','02/04/1987',' 034 43 523 95','egestas@egestasSed.net',(SELECT digest('M01I','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-51','Taylor','Hilary','Feminin','04/05/1985',' 034 27 651 44','sit.amet.nulla@lacusvariuset.com',(SELECT digest('T93T','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-52','Jones','Kirsten','Feminin','16/04/1966','032 40 277 81','ultrices.posuere.cubilia@Quisque.co.uk',(SELECT digest('V56Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-53','Bishop','Hedwig','Feminin','24/01/1989','032 12 518 41','rutrum.lorem@magnaaneque.ca',(SELECT digest('I55Y','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-54','Morris','Zena','Masculin','21/06/2010',' 034 26 399 19','mattis.ornare.lectus@justoeu.ca',(SELECT digest('H33G','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-55','Landry','Guy','Feminin','04/02/2013',' 033 84 299 47','erat@adipiscinglacusUt.com',(SELECT digest('A19U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-56','Gaines','Felicia','Feminin','01/07/1965',' 033 54 542 56','magna@Pellentesqueut.ca',(SELECT digest('L27A','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-57','Meyer','Whoopi','Feminin','19/04/2010','032 15 853 36','et.risus.Quisque@quistristique.edu',(SELECT digest('W78G','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-58','Tran','Nichole','Feminin','18/10/1988',' 034 00 629 58','mi.ac@Namporttitorscelerisque.org',(SELECT digest('Q94B','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-59','Pollard','Vanna','Feminin','05/01/2003',' 033 99 733 79','lectus.justo@nullaIn.net',(SELECT digest('N16U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-60','Bass','Gwendolyn','Masculin','16/05/2004','032 74 543 63','neque.venenatis.lacus@tempor.org',(SELECT digest('G96P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-61','Fox','Yardley','Masculin','31/07/2010',' 034 93 528 63','adipiscing.elit@ametconsectetueradipiscing.co.uk',(SELECT digest('V36I','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-62','Aguirre','Winter','Feminin','12/05/1970',' 033 38 897 37','vel.venenatis.vel@elitEtiamlaoreet.org',(SELECT digest('C51L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-63','Burks','Pearl','Masculin','17/05/1990',' 033 43 104 32','non.hendrerit@Crasconvallisconvallis.co.uk',(SELECT digest('P00M','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-64','Davidson','Myra','Masculin','13/07/1986','032 73 911 96','egestas@CraspellentesqueSed.ca',(SELECT digest('L56L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-65','Munoz','Hammett','Masculin','26/03/1984',' 034 50 131 71','mi.tempor@consectetuer.com',(SELECT digest('S87L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-66','Finch','Ivana','Feminin','12/12/2006',' 034 07 625 59','Nullam.enim@nec.net',(SELECT digest('B49B','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-67','Gibbs','Katelyn','Feminin','11/04/1977',' 033 09 015 20','Donec.vitae@mi.co.uk',(SELECT digest('M13O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-68','Pacheco','Francis','Feminin','11/09/1964',' 034 02 895 95','malesuada@purusDuis.ca',(SELECT digest('M70J','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-69','Rich','Branden','Feminin','23/05/1980','032 38 228 95','feugiat@eratvelpede.edu',(SELECT digest('G46N','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-70','Hester','Kamal','Masculin','28/02/2003','032 04 739 23','Maecenas.libero.est@mipede.co.uk',(SELECT digest('G01Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-71','Rosales','Lawrence','Feminin','03/10/1975','032 04 612 38','vel.arcu@euismod.co.uk',(SELECT digest('R79S','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-72','Barr','Erasmus','Masculin','15/06/2014',' 034 44 488 38','In.condimentum.Donec@acliberonec.ca',(SELECT digest('S88T','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-73','Hayden','Jessica','Feminin','11/10/1966',' 034 65 141 15','at.nisi@ategestasa.ca',(SELECT digest('M79E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-74','Lamb','Kamal','Feminin','30/04/1989',' 034 07 644 85','tincidunt.neque.vitae@in.com',(SELECT digest('A54B','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-75','Fitzpatrick','Megan','Feminin','15/12/2010',' 034 52 228 72','Donec@necante.ca',(SELECT digest('F88X','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-76','Pierce','Kaden','Feminin','14/11/2004',' 033 94 411 85','semper@Donecnonjusto.edu',(SELECT digest('H05H','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-77','Hines','Leroy','Masculin','25/09/1980',' 033 11 570 45','sodales.at@euelit.co.uk',(SELECT digest('M61R','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-78','Hess','Ainsley','Feminin','26/07/1965',' 033 04 918 57','Duis.sit.amet@Sedeu.net',(SELECT digest('Z01O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-79','Rutledge','Rama','Feminin','21/02/1994','032 37 662 27','at.auctor.ullamcorper@Nullam.org',(SELECT digest('V97D','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-80','Butler','Vernon','Feminin','25/07/2000',' 033 73 488 79','Duis.at@estvitaesodales.com',(SELECT digest('J51Q','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-81','Gilbert','Piper','Feminin','28/09/2000',' 033 04 763 41','lectus.Cum.sociis@nec.org',(SELECT digest('V40E','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-82','Vazquez','Eagan','Masculin','09/06/1970','032 95 684 36','fames.ac@ipsum.com',(SELECT digest('A62R','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-83','White','Kameko','Feminin','08/04/2010',' 033 85 018 86','iaculis@Quisqueporttitor.com',(SELECT digest('R38D','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-84','Dunlap','Evan','Masculin','27/03/1998',' 033 53 751 76','lacus@sagittis.net',(SELECT digest('A78G','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-85','Cote','Joel','Feminin','07/09/1970',' 033 37 960 51','ullamcorper.Duis.at@dictum.co.uk',(SELECT digest('U77L','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-86','Conner','Amelia','Masculin','14/05/1979',' 034 36 884 50','lectus@Maecenasornareegestas.ca',(SELECT digest('A51Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-87','Perez','Kennedy','Feminin','01/08/1981',' 033 32 090 68','purus.Nullam@nulla.edu',(SELECT digest('N73U','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-88','Hardy','Alana','Feminin','03/10/1992','032 66 324 87','blandit.Nam@venenatislacusEtiam.edu',(SELECT digest('Q53Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-89','Ratliff','Kasper','Masculin','06/02/1980',' 033 98 087 28','eget.dictum.placerat@non.net',(SELECT digest('P25W','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-90','Mcclure','Gretchen','Feminin','08/08/2014',' 033 50 875 79','diam.luctus@quis.edu',(SELECT digest('S97P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-91','Montoya','Hunter','Feminin','14/11/1990',' 034 42 600 79','congue@egetodio.edu',(SELECT digest('I33I','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-92','Welch','Tiger','Masculin','01/12/1994',' 034 22 190 60','vel.turpis.Aliquam@vulputaterisus.ca',(SELECT digest('L20Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-93','Brennan','Hanna','Masculin','20/05/1998',' 034 85 869 77','Aenean.massa@velitCras.ca',(SELECT digest('E67F','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-94','Barron','Destiny','Feminin','20/10/1981','032 64 168 69','egestas.Duis@sociis.com',(SELECT digest('A92Q','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-95','Carter','Nicholas','Masculin','26/10/1961','032 32 502 56','neque.venenatis@nisi.ca',(SELECT digest('S25Z','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-96','Malone','Maryam','Feminin','02/08/1980',' 034 99 527 04','Morbi.metus.Vivamus@Vestibulum.edu',(SELECT digest('D87O','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-97','Torres','Charlotte','Feminin','14/08/1970',' 033 95 306 41','turpis.non@ametornare.org',(SELECT digest('G86P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-98','Baird','Alec','Feminin','01/03/1982',' 034 32 598 19','dapibus.id@Sed.com',(SELECT digest('E33P','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-99','Ford','Jocelyn','Masculin','27/09/1979',' 033 88 212 59','eu@ipsumsodales.co.uk',(SELECT digest('R36B','SHA1')),'PRF-1');
INSERT INTO Utilisateurs (id,nom,prenom,sexe,dateDeNaissance,telephone,email,motDePasse,idProfil) VALUES ('USR-100','Gates','Harrison','Masculin','01/12/1987','032 78 709 69','admin@gmail.com',(SELECT digest('C02X','SHA1')),'PRF-2');


