package model;
import Generalise.FonctionReflect;
import connection.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Billet {
    String id;
    int etat;
    Double total;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEtat() {
        return this.etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Double getTotal() {
        return this.total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public Billet(String id, int etat, Double total) {
        this.id = id;
        this.etat = etat;
        this.total = total;
    }
    public Billet(){
        
    }
    public void insertDetails(Reservation reservation, String idPlace,Connection c) throws Exception{
        try{
            String idBD;
            idBD = FonctionReflect.generateId("idBilletDetails","BLLD-", c);
            Voyage v= reservation.getByid(reservation.getIdVoyage(), c);
            Trajet t= v.getByid(v.getIdTrajet(), c);
            Vehicule vehicl =v.getVehicule(v.getIdVehicule(), c);
            Category cat=vehicl.getCategorie(vehicl.getIdCategory(), c);
             Double prix=t.getPrix()+cat.getPourcentage();
            BilletDetails newBillet = new BilletDetails(idBD,reservation.getId(),this.getId(),prix);
            FonctionReflect.insert("Billet", newBillet, c);
            
        }catch(Exception exp){
            throw exp;
        }
    }
    public int updateBillet(Billet billet, String id,DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql = "update Billet set id = '"+billet.getId()+"', set etat ="+billet.getEtat()+", set total = "+billet.getTotal()+" ";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch (SQLException e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}