package model;

import Generalise.FonctionReflect;
import connection.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Reservation {
    String id;
    String idVoyage;
    String idPlace;
    String idUtilisateurs;
    Timestamp daty;
    int etat;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVoyage() {
        return this.idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getIdPlace() {
        return this.idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }

    public String getIdUtilisateurs() {
        return this.idUtilisateurs;
    }

    public void setIdUtilisateurs(String idUtilisateurs) {
        this.idUtilisateurs = idUtilisateurs;
    }

    public Timestamp getDaty() {
        return this.daty;
    }

    public void setDaty(Timestamp daty) {
        this.daty = daty;
    }

    public int getEtat() {
        return this.etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Reservation(String id, String idVoyage, String idPlace, String idUtilisateurs, Timestamp daty, int etat) {
        this.id = id;
        this.idVoyage = idVoyage;
        this.idPlace = idPlace;
        this.idUtilisateurs = idUtilisateurs;
        this.daty = daty;
        this.etat = etat;
    }
        public Voyage getByid(String idVoyage,Connection c) throws Exception{
        Voyage response = new Voyage();
        try{
            List<Object> obj_list = FonctionReflect.selectWithCondition("Voyage",response,"WHERE idVoyage = '"+idVoyage+"'", c);
            response = (Voyage)obj_list.get(0);
        }catch(Exception e){
            throw e;
        }
        return response;
    }
    public Reservation(){
        
    }
    public int updateReservarion (Reservation reservation, String id, DBConnection c) throws Exception {
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql ="update Reservation set id = '"+reservation.getId()+"', set idVoyage = '"+reservation.getIdVoyage()+"', set idPlace = '"+reservation.getIdPlace()+"', set idUtilisateurs = '"+reservation.getIdUtilisateurs()+"', set daty = '"+reservation.getDaty()+"', set etat="+reservation.getEtat()+" ";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(SQLException e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}