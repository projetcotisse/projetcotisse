package model;

import Generalise.FonctionReflect;
import connection.DBConnection;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Trajet {
    String id;
    String depart;
    String arrivee;
    String heure;
    Float prix;

public Trajet(){
    
}

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepart() {
        return this.depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return this.arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public String getHeure() {
        return this.heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public Float getPrix() {
        return this.prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Trajet(String id, String depart, String arrivee, String heure, Float prix) {
        this.id = id;
        this.depart = depart;
        this.arrivee = arrivee;
        this.heure = heure;
        this.prix = prix;
    }
    public String getLieu(String idLieu) throws Exception{
        DBConnection connection = new DBConnection();
        String lieu = "";
        try{
            Object[] objList = FonctionReflect.selectWithCondition("Lieu", new Lieu(), "WHERE lieu like '"+idLieu+"'", connection.getConnection()).toArray();
            lieu = (String) objList[0];
        }catch(Exception exp){
            throw exp;
        }finally{
            connection.clear();
        }
        return lieu;
    }
    public void createT (Trajet trajet, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Trajet",trajet,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
                
    public int deleteT(String id, DBConnection c) throws Exception{
                boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Trajet where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
                    
    public Trajet[] getAllTrajet(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Trajet> listeTrajet= (List<Trajet>) (Object) FonctionReflect.select("Trajet", new Trajet(), c.getConnection());
            Trajet[] resultat = new Trajet[listeTrajet.size()];
            resultat=listeTrajet.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}