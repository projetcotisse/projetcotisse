package model;
import Generalise.FonctionReflect;
import connection.DBConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Lieu {
    String id;
    String lieu;

    public Lieu() {
       
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLieu() {
        return this.lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Lieu(String id, String lieu) {
        this.id = id;
        this.lieu = lieu;
    }
    public void createL (Lieu lieu, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Lieu",lieu,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
    public int deleteL(String id, DBConnection c) throws Exception{
                boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Lieu where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
    
    public Lieu[] getAllLieu(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Lieu> listeLieu = (List<Lieu>) (Object) FonctionReflect.select("Lieu", new Lieu(), c.getConnection());
            Lieu[] resultat = new Lieu[listeLieu.size()];
            resultat = listeLieu.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}
