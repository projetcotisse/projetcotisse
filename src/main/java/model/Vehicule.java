package model;


import Generalise.FonctionReflect;
import connection.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Vehicule {
    String immatriculation;
    int place;
    String idCategory;

public Vehicule(){
    
}

    public String getImmatriculation() {
        return this.immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public int getPlace() {
        return this.place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public Vehicule(String immatriculation, int place, String idCategory) {
        this.immatriculation = immatriculation;
        this.place = place;
        this.idCategory = idCategory;
    }
        public Category getCategorie(String idCategory, Connection c) throws Exception{
        Category response =new Category();
        try {
            List<Object> objListCategorie= FonctionReflect.selectWithCondition("Categorie", response, "WHERE idCategorie ='"+idCategory+"'", c);
            response=(Category)objListCategorie.get(0);
        } catch (Exception e) {
            throw e;
        }
        return response;
    }
        public void createV (Vehicule vehicule, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Vehicule",vehicule,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
    
    public int deleteV(String id, DBConnection c) throws Exception{
                boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from vehicule where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
    
    public Vehicule[] getAllVehicule(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Vehicule> listeVehicule = (List<Vehicule>) (Object) FonctionReflect.select("Vehicule", new Vehicule(), c.getConnection());
            Vehicule[] resultat = new Vehicule[listeVehicule.size()];
            resultat = listeVehicule.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}
