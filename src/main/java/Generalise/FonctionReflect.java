/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generalise;

import connection.DBConnection;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.Timestamp;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Mitia Andria
 */
public class FonctionReflect {
    public static Double sommeGeneral(Object[] tableau,String attribSommer) throws Exception {
        Class classe=tableau[0].getClass();
        Method methode=classe.getMethod("get"+attribSommer,null);
        Double result=new Double(0);
        for(int i=0;i<tableau.length;i++) {
            result+=(Double) methode.invoke(tableau[i],null);
        }
        return result;
    }
    
    public static Double sommeGeneral2(Object[] tableau,String attribSommer,DBConnection connection) throws Exception {
        Class classe=tableau[0].getClass();
        Method methode=classe.getMethod("get"+attribSommer,connection.getClass());
        Double result=new Double(0);
        for(int i=0;i<tableau.length;i++) {
            result+=(Double) methode.invoke(tableau[i],null);
        }
        return result;
    }
    
    public static List<Object> select(String tableName, Object reference, Connection connection) throws Exception {
		if (connection == null) {
			throw new Exception("Error to Select on " + tableName + ": Connection is null");
		}
		if (connection.isClosed()) {
			throw new Exception("Error to Select on " + tableName + ": Connection is closed");
		}
		List<Object> result = new ArrayList<Object>();
		String request = "SELECT * FROM ".concat(tableName);
		Field[] fields = reference.getClass().getDeclaredFields();
		List<Field> commonToDatabase = new ArrayList<Field>();
		List<String> columnCommonToClass = new ArrayList<String>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultSetMetaData = null;
                //System.out.println(request);
		try {
			preparedStatement = connection.prepareStatement(request);
			resultSet = preparedStatement.executeQuery();
			resultSetMetaData = resultSet.getMetaData();
                        //System.out.println("taille field "+fields.length);
                        
			for (int j = 0; j < fields.length; j++) {
				for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
					if (resultSetMetaData.getColumnName(i + 1).compareToIgnoreCase(fields[j].getName()) == 0) {
						commonToDatabase.add(fields[j]);
						columnCommonToClass.add(resultSetMetaData.getColumnName(i + 1));
						break;
					}
				}
			}
			while (resultSet.next()) {
				Object object = reference.getClass().newInstance();
				for (int i = 0; i < columnCommonToClass.size(); i++) {
					Method method = Utils.getSettersOf(object, columnCommonToClass.get(i));
					if (commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("int") == 0
							|| commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("Integer") == 0) {
						method.invoke(object, resultSet.getInt(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("double") == 0) {
						method.invoke(object, resultSet.getDouble(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareTo("Timestamp") == 0) {
						method.invoke(object, resultSet.getTimestamp(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareTo("Date") == 0) {
						method.invoke(object, resultSet.getDate(columnCommonToClass.get(i)));
					} else {
						method.invoke(object, resultSet.getString(columnCommonToClass.get(i)));
					}
				}
				result.add(object);
			}
		} catch (Exception e) {
			throw new Exception("Error on select on the table " + tableName + ":" + e.getMessage());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}

		}
		return result;
	}

        public static String getClassName(Class c){
            String name = c.getName();
            int lastIndex = name.lastIndexOf(".");
            String className = name.substring(lastIndex+1);
            return className;
        }
	public static List<Object> selectWithCondition(String tableName, Object reference, String next,
			Connection connection) throws Exception {
		if (connection == null) {
			throw new Exception("Error to Select on " + tableName + ": Connection is null");
		}
		if (connection.isClosed()) {
			throw new Exception("Error to Select on " + tableName + ": Connection is closed");
		}
		List<Object> result = new ArrayList<Object>();
		String request = "SELECT * FROM ".concat(tableName).concat(" ").concat(next);
		//System.out.println(request);
		List<Field> liste = new ArrayList();
		Class temp = reference.getClass();
		int tour = 0;
		while (!temp.equals(new Object().getClass())) {
			Field[] temporaire = temp.getDeclaredFields();
			for (int i = 0; i < temporaire.length; i++) {
				liste.add(temporaire[i]);
			}
			temp = temp.getSuperclass();
			if (++tour > 2) {
				break;
			}
		}
		Field[] fields = liste.toArray(new Field[0]);
		List<Field> commonToDatabase = new ArrayList<Field>();
		List<String> columnCommonToClass = new ArrayList<String>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultSetMetaData = null;
		try {
			preparedStatement = connection.prepareStatement(request);
			resultSet = preparedStatement.executeQuery();
			resultSetMetaData = resultSet.getMetaData();
			for (int j = 0; j < fields.length; j++) {
				for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
					if (resultSetMetaData.getColumnName(i + 1).compareToIgnoreCase(fields[j].getName()) == 0) {
						commonToDatabase.add(fields[j]);
						columnCommonToClass.add(resultSetMetaData.getColumnName(i + 1));
						break;
					}
				}
			}
			while (resultSet.next()) {
				Object object = reference.getClass().newInstance();
				for (int i = 0; i < columnCommonToClass.size(); i++) {
					Method method = Utils.getSettersOf(object, columnCommonToClass.get(i));
					if (commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("int") == 0
							|| commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("Integer") == 0) {
						method.invoke(object, resultSet.getInt(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareToIgnoreCase("double") == 0) {
						method.invoke(object, resultSet.getDouble(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareTo("Timestamp") == 0) {
						method.invoke(object, resultSet.getTimestamp(columnCommonToClass.get(i)));
					} else if (commonToDatabase.get(i).getType().getSimpleName().compareTo("Date") == 0) {
						method.invoke(object, resultSet.getDate(columnCommonToClass.get(i)));
					} else {
						method.invoke(object, resultSet.getString(columnCommonToClass.get(i)));
					}
				}
				result.add(object);
			}
		} catch (Exception e) {
			throw new Exception("Error on select on the table " + tableName + ":" + e.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return result;
	}

	public static void insert(String tableName, Object object, Connection connection) throws Exception {
		if (connection == null) {
			throw new Exception("Error to Insert on " + tableName + ": Connection is null");
		}
		if (connection.isClosed()) {
			throw new Exception("Error to Insert on " + tableName + ": Connection is closed");
		}
		DatabaseMetaData databaseMetaData = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try {
			Field[] fields = object.getClass().getDeclaredFields();
			databaseMetaData = connection.getMetaData();
			resultSet = databaseMetaData.getColumns(null, null, tableName, null);
			List<String> colums = new ArrayList<>();
			while (resultSet.next()) {
				colums.add(resultSet.getString("COLUMN_NAME"));
			}
			List<String> columnCommonToObject = new ArrayList<>();
			List<Field> fieldCommonToDB = new ArrayList<>();
			for (Field field : fields) {
				for (String column : colums) {
					if (field.getName().compareToIgnoreCase(column) == 0) {
						columnCommonToObject.add(column);
						fieldCommonToDB.add(field);
						break;
					}
				}
			}
			String column = "";
			String value = "";
			resultSet.close();
			for (int i = 0; i < columnCommonToObject.size(); i++) {
				column += "" + columnCommonToObject.get(i);
				Method method = Utils.getGettersOf(object, columnCommonToObject.get(i));
				Field field = fieldCommonToDB.get(i);
				if (method.invoke(object) == null) {
					value += "null";
				} else {
					if (field.getType().getSimpleName().compareToIgnoreCase("double") == 0
							|| field.getType().getSimpleName().compareToIgnoreCase("int") == 0
							|| field.getType().getSimpleName().compareToIgnoreCase("Integer") == 0) {
						value += String.valueOf(method.invoke(object));
					} else if (field.getType().getSimpleName().compareToIgnoreCase("Date") == 0) {
						value += "'" + ((Date) method.invoke(object)).toString() + "'";
					} else if (field.getType().getSimpleName().compareToIgnoreCase("Timestamp") == 0) {
						value += "'" + ((Timestamp) method.invoke(object)).toString() + "'";
					} else {
						value += "'" + String.valueOf(method.invoke(object))+"'";
					}
				}
				if (i != columnCommonToObject.size() - 1) {
					column += ",";
					value += ",";
				}
			}
			String request = "INSERT INTO " + tableName + " (" + column + " ) VALUES (" + value + ")";
			System.out.println(request);
			preparedStatement = connection.prepareStatement(request);
			preparedStatement.execute();
		} catch (Exception e) {
			//e.printStackTrace();
			throw new Exception("Error to Insert on " + tableName + ":" + e.getMessage());
		} finally {
			
			preparedStatement.cancel();
		}

	}

	public static String generateId(String sequenceName, String sequenceInitial, Connection connexion) throws Exception {
		String requestGenerateSequence = "select nextval('"+sequenceName+"')";
		Statement s = connexion.createStatement();
		ResultSet sequence = s.executeQuery(requestGenerateSequence);
		String indiceSequence = "";
		while (sequence.next()) {
			indiceSequence = sequence.getString(1);
		}
		sequence.close();
		s.close();
		String result = sequenceInitial.concat(indiceSequence);
		return result;
	}
}
