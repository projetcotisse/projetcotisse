package connection;
import java.sql.*;
import java.lang.reflect.*;
public class DBConnection{
	Connection co;
	Statement stmt;
	ResultSet rs;
	public Connection getConnection(){
		return this.co;
	}
	public void clear()throws Exception{
		co.close();
		stmt.close();
	}
	public Statement getStatement(){
		return this.stmt;
	}
	public DBConnection(){
            
            try{
                Class.forName("org.postgresql.Driver");
                this.co = DriverManager
                   .getConnection("jdbc:postgresql://localhost:5432/cotisse",
                   "postgres", "1234");
		this.stmt = this.co.createStatement();
                System.out.println("connecte !");
            }catch(Exception e){
                
            }	
	}
	public void setAutoCommit(boolean value)throws Exception{
		this.co.setAutoCommit(value);
	}
	public void commit()throws Exception{
		this.co.commit();
	}
	public void rollback()throws Exception{
		this.co.rollback();
	}
}