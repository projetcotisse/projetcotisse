/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fonction;

import Generalise.FonctionReflect;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import model.Utilisateurs;

/**
 *
 * @author mamai
 */
public class Fonction {
    public String generateMdp( String pass, Connection connexion) throws Exception {
		String requestGenerateSequence = "SELECT digest('"+pass+"','SHA1')";
		Statement s = connexion.createStatement();
		ResultSet sequence = s.executeQuery(requestGenerateSequence);
		String indiceSequence = "";
		while (sequence.next()) {
			indiceSequence = sequence.getString(1);
		}
		sequence.close();
		s.close();
                
		return indiceSequence;
	}
    public Utilisateurs login(String username,String password,Connection c) throws Exception{
        try{
            String mdp = this.generateMdp(password, c);
            List<Object> objList = FonctionReflect.selectWithCondition("Utilisateurs", new Utilisateurs(), "WHERE email = '"+username+"' and motDePasse = '"+mdp +"'",c);
            if(objList.isEmpty()){
                throw new Exception("Oops! Utilisateurs introuvable.");
            }else{
                Utilisateurs response = (Utilisateurs) objList.get(0);
                return response;
            }
        }catch(Exception exp){
            throw exp;
        }
    }
    
    public Utilisateurs inscription(String nom,String prenom,String sexe,String dateDeNaissance,String telephone,String email,String motDePasse,Connection c) throws Exception{
        try{
            String id = FonctionReflect.generateId("idUtilisateurs", "USR-",c);
            Date dNaissance = Date.valueOf(dateDeNaissance);
            Utilisateurs insertObj = new Utilisateurs(id,nom,prenom,sexe,dNaissance,telephone,email,motDePasse,"PRF-1");
            int doublon = insertObj.verifDoublon(c);
            if(doublon == 1){
                throw new Exception("Cette adresse mail est déja utilisée sur ce site.");
            }
            else{
                try{
                     FonctionReflect.insert("Utilisateurs", insertObj, c);
                }catch(Exception exep){
                    throw exep;
                }
               return insertObj;
                
            }
        }catch(Exception exp){
            throw exp;
        }
        
    }
}
