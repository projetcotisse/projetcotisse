/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fonction;

import Connection.DBConnection;
import Generalise.FonctionReflect;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import javax.ws.rs.Consumes;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.BilletDetails;
import model.Place;
import model.Reservation;
import model.Trajet;
import model.Utilisateurs;

/**
 *
 * @author mamai
 */
 @Path("fonction")
public class Fonction {
     /*
        login class utilisateurs
        inscription class utilisateurs
        mesReservation class utilisateurs
        reserverPlace class fonction
     */
     /*
            Reserver :
                add place /done
                add reservation /done
                add billetDetails
                add billet
        */
    public String generateMdp( String pass, Connection connexion) throws Exception {
		String requestGenerateSequence = "SELECT digest('"+pass+"','SHA1')";
		Statement s = connexion.createStatement();
		ResultSet sequence = s.executeQuery(requestGenerateSequence);
		String indiceSequence = "";
		while (sequence.next()) {
			indiceSequence = sequence.getString(1);
		}
		sequence.close();
		s.close();
                
		return indiceSequence;
	}
    
    public Trajet getTrajetByid(String id , Connection connexion) throws Exception{
        try{
            Trajet reference = new Trajet();
            List<Object> list_obj = FonctionReflect.selectWithCondition("Trajet",reference," WHERE id = '"+id+"'", connexion);
            reference = (Trajet)list_obj.get(0);
            return reference;
        }catch(Exception exp){
            throw exp;
        }
    }
    
    public int verifPlace(String immatriculation , String numPlace,Connection connexion) throws Exception{
        try{
            Place reference = new Place();
            List<Object> list_obj = FonctionReflect.selectWithCondition("Place",reference," WHERE immatriculation = '"+immatriculation+"' and numPlace = '"+numPlace+"'", connexion);
            reference = (Place)list_obj.get(0);
            if(reference.getEtat() == 10){
                throw new Exception("Place deja résérver.");
            }else{
                return 1;
            }
        }catch(Exception exp){
            throw exp;
        }
    }
    
    public void reserverPlace(String immatriculation , String numPlace,Connection connexion)throws Exception{
        try{
            String sql ="update Place set etat = 10 where immatriculation = '"+immatriculation+"' and numPlace = '"+numPlace+"'";
            Statement stmt = connexion.createStatement();
            stmt.executeUpdate(sql);
        }
        catch(SQLException e){
            throw e;
        }
    }
    
    public void addReservation(String idVoyage, String idPlace, String idUtilisateurs,Connection connexion)throws Exception{
        try{
            java.util.Date date = new java.util.Date();
            Timestamp ts = new Timestamp(date.getTime());
            String id = FonctionReflect.generateId("idReservation", "RS-", connexion);
            Reservation reserv = new Reservation(id,idVoyage,idPlace,idUtilisateurs,ts, 1) ;
            FonctionReflect.insert("Reservation", reserv, connexion);
        }catch(Exception e){
            throw e;
        }
    }
    public void addBilletDetails( String idReservation, String idBillet, Double prix,Connection connexion) throws Exception{
        try{
            String id = FonctionReflect.generateId("idReservation", "RS-", connexion);
            BilletDetails bd = new BilletDetails( id,  idReservation,  idBillet,  prix);
            FonctionReflect.insert("BilletDetails", bd, connexion);
        }catch(Exception exp){
            throw exp;
        }
                    
    }
//    @Path("/login")
//    @POST
//    public Utilisateurs login(String username,String password) throws Exception{
//        DBConnection c = new DBConnection();
//        Utilisateurs u = new Utilisateurs();
//        try{
//             u = u.login(username,password,c.getConnection());
//             
//        }catch(Exception exp){
//            throw exp;
//        }finally{
//            c.clear();
//        }
//        return u;
//    }
    
    
//    @Path("/inscription")
//    @POST
//    
//    public Utilisateurs inscription(String nom,String prenom,String sexe,String dateDeNaissance,String telephone,String email,String motDePasse,Connection c) throws Exception{
//        try{
//            String id = FonctionReflect.generateId("idUtilisateurs", "USR-",c);
//            Date dNaissance = Date.valueOf(dateDeNaissance);
//            Utilisateurs insertObj = new Utilisateurs(id,nom,prenom,sexe,dNaissance,telephone,email,motDePasse,"PRF-1");
//            int doublon = insertObj.verifDoublon(c);
//            if(doublon == 1){
//                throw new Exception("Cette adresse mail est déja utilisée sur ce site.");
//            }
//            else{
//                try{
//                     FonctionReflect.insert("Utilisateurs", insertObj, c);
//                }catch(Exception exep){
//                    throw exep;
//                }
//               return insertObj;
//                
//            }
//        }catch(Exception exp){
//            throw exp;
//        }
//        
//    }
}
