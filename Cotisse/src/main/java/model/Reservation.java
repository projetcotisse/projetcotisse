package model;

import Generalise.FonctionReflect;
import Connection.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import webServiceClass.Reserver;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Reservation {
    String id;
    String idVoyage;
    String idPlace;
    String iUtilisateurs;
    Timestamp daty;
    int etat;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVoyage() {
        return this.idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getIdPlace() {
        return this.idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }

    public String getIUtilisateurs() {
        return this.iUtilisateurs;
    }

    public void setIUtilisateurs(String iUtilisateurs) {
        this.iUtilisateurs = iUtilisateurs;
    }

    public Timestamp getDaty() {
        return this.daty;
    }

    public void setDaty(Timestamp daty) {
        this.daty = daty;
    }

    public int getEtat() {
        return this.etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Reservation(String id, String idVoyage, String idPlace, String iUtilisateurs, Timestamp daty, int etat) {
        this.id = id;
        this.idVoyage = idVoyage;
        this.idPlace = idPlace;
        this.iUtilisateurs = iUtilisateurs;
        this.daty = daty;
        this.etat = etat;
    }
    public Reservation(){        
    }
    public Voyage getByid(String idVoyage,Connection c) throws Exception{
        Voyage response = new Voyage();
        try{
            List<Object> obj_list = FonctionReflect.selectWithCondition("Voyage",response,"WHERE idVoyage = '"+idVoyage+"'", c);
            response = (Voyage)obj_list.get(0);
        }catch(Exception e){
            throw e;
        }
        return response;
    }
    public int updateReservarion (Reservation reservation, String id, DBConnection c) throws Exception {
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql ="update Reservation set id = '"+reservation.getId()+"', idVoyage = '"+reservation.getIdVoyage()+"', idPlace = '"+reservation.getIdPlace()+"', iUtilisateurs = '"+reservation.getIUtilisateurs()+"', daty = '"+reservation.getDaty()+"', etat="+reservation.getEtat()+" ";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(SQLException e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
        public int deleteRes(String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Reservation where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
        public Reservation[] getAllReservation(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Reservation> listeReservation = (List<Reservation>) (Object) FonctionReflect.select("Reservation", new Reservation(), c.getConnection());
            Reservation[] resultat = new Reservation[listeReservation.size()];
            resultat = listeReservation.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
        public Reserver[] getAllReserver(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Reserver> listeReserver = (List<Reserver>) (Object) FonctionReflect.select("Reserver", new Reserver(), c.getConnection());
            Reserver[] resultat = new Reserver[listeReserver.size()];
            resultat = listeReserver.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}
