package model;
import Generalise.FonctionReflect;
import Connection.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Voyage {
    String id;
    String idVehicule;
    String idTrajet;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVehicule() {
        return this.idVehicule;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getIdTrajet() {
        return this.idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public Voyage(String id, String idVehicule, String idTrajet) {
        this.id = id;
        this.idVehicule = idVehicule;
        this.idTrajet = idTrajet;
    }
    public Voyage(){
    
    }
    public Trajet getByid(String idTrajet,Connection c) throws Exception{
        Trajet response = new Trajet();
        try{
            List<Object> obj_list = FonctionReflect.selectWithCondition("Trajet",response,"WHERE idTrajet = '"+idTrajet+"'", c);
            response = (Trajet)obj_list.get(0);
        }catch(Exception e){
            throw e;
        }
        return response;
    }
    public Vehicule getVehicule(String idVehicule, Connection c) throws Exception{
        Vehicule response = new Vehicule();
        
        try {
            List<Object> objListVehicule= FonctionReflect.selectWithCondition("Vehicule", response,"WHERE immatriculation= '"+idVehicule+"'", c);
            response=(Vehicule)objListVehicule.get(0);
        } catch (Exception e) {
            throw e;
        }
        return response;
    }
    public void createVy (Voyage voyage, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Voyeage",voyage,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
        public int deleteVy(String id, DBConnection c) throws Exception{
                boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Voyage where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
        public Voyage[] getAllVoyage(DBConnection c) throws Exception{
            boolean connection = false;
            if(c == null){
                c = new DBConnection();
                connection = true;
            }
            try{
                List<Voyage> listeVoyage= (List<Voyage>) (Object) FonctionReflect.select("Voyage", new Voyage(), c.getConnection());
                Voyage[] resultat = new Voyage[listeVoyage.size()];
                resultat=listeVoyage.toArray(resultat);
                return resultat;
            }
            catch (Exception e){
                throw e;
            }
            finally{
                if(connection == true){
                    c.clear();
                }
            }
        }
        
        public int updateVoyage(Voyage voyage, String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql = "update Voyage set id = '"+voyage.getId()+"', set idVehicule = '"+voyage.getIdVehicule()+"' , set idTrajet = '"+voyage.getIdTrajet()+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch (SQLException e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}