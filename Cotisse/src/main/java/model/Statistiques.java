/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andya
 */
public class Statistiques {
    String label;//idtrajet , category et autre
    int count;
    String daty;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDaty() {
        return daty;
    }

    public void setDaty(String daty) {
        this.daty = daty;
    }

    public Statistiques() {
    }

    public Statistiques(String label, int count, String daty) {
        this.label = label;
        this.count = count;
        this.daty = daty;
    }    
    /**
     * 
     * @param connexion
     * @return
     * @throws Exception 
     */
    public Statistiques[] getStatCategoryGeneral(Connection connexion) throws Exception{
        try{
            
            String requestGenerateSequence = "select category.category,count(voyage.idtrajet) from reservation join voyage on reservation.idvoyage = voyage.id join vehicule on vehicule.immatriculation = voyage.idvehicule join category on vehicule.idcategory = category.id group by category.category";
            Statement s = connexion.createStatement();
            ResultSet sequence = s.executeQuery(requestGenerateSequence);
            List<Statistiques> listStat = new ArrayList<Statistiques>();
            while (sequence.next()) {
                   Statistiques stat = new Statistiques(sequence.getString("category"),sequence.getInt("count"),null);
                   listStat.add(stat);
            }
            sequence.close();
            s.close();
            Statistiques[] response = new Statistiques[listStat.size()];
            response = listStat.toArray(response);
            return response;
        }catch(Exception exp){
            throw exp;
        }
    }
    public Statistiques[] getStatCategory(String category,Connection connexion) throws Exception{
        try{
                
            String requestGenerateSequence = "select category.category,count(voyage.idtrajet),reservation.daty from reservation join voyage on reservation.idvoyage = voyage.id join vehicule on vehicule.immatriculation = voyage.idvehicule join category on vehicule.idcategory = category.id where category = '"+ category+"' group by category.category, reservation.daty";
            Statement s = connexion.createStatement();
            ResultSet sequence = s.executeQuery(requestGenerateSequence);
            List<Statistiques> listStat = new ArrayList<Statistiques>();
            while (sequence.next()) {
                   Statistiques stat = new Statistiques(sequence.getString("category"),sequence.getInt("count"),sequence.getDate("daty").toString());
                   listStat.add(stat);
            }
            sequence.close();
            s.close();
            Statistiques[] response = new Statistiques[listStat.size()];
            response = listStat.toArray(response);
            return response;
        }catch(Exception exp){
            throw exp;
        }
    }
    public Statistiques[] getStatTrajetGeneral(Connection connexion) throws Exception{
        try{
            
            String requestGenerateSequence = "select voyage.idTrajet,count(voyage.idtrajet) from reservation join voyage on reservation.idvoyage = voyage.id join trajet on trajet.id = voyage.idtrajet group by voyage.idtrajet";
            Statement s = connexion.createStatement();
            ResultSet sequence = s.executeQuery(requestGenerateSequence);
            List<Statistiques> listStat = new ArrayList<Statistiques>();
            while (sequence.next()) {
                   Statistiques stat = new Statistiques(sequence.getString("idtrajet"),sequence.getInt("count"),null);
                   listStat.add(stat);
            }
            sequence.close();
            s.close();
            Statistiques[] response = new Statistiques[listStat.size()];
            response = listStat.toArray(response);
            return response;
        }catch(Exception exp){
            throw exp;
        }
    }
    public Statistiques[] getStatTrajet(String idtrajet,Connection connexion) throws Exception{
        try{
            
            String requestGenerateSequence = "select voyage.idTrajet,count(voyage.idtrajet),reservation.daty from reservation join voyage on reservation.idvoyage = voyage.id join trajet on trajet.id = voyage.idtrajet where idtrajet = '"+idtrajet+"' group by voyage.idtrajet, reservation.daty";
            Statement s = connexion.createStatement();
            ResultSet sequence = s.executeQuery(requestGenerateSequence);
            List<Statistiques> listStat = new ArrayList<Statistiques>();
            while (sequence.next()) {
                   Statistiques stat = new Statistiques(sequence.getString("idtrajet"),sequence.getInt("count"),sequence.getDate("daty").toString());
                   listStat.add(stat);
            }
            sequence.close();
            s.close();
            Statistiques[] response = new Statistiques[listStat.size()];
            response = listStat.toArray(response);
            return response;
        }catch(Exception exp){
            throw exp;
        }
    }
    public List<Statistiques[]> getStats(Connection connexion) throws Exception {
        try{
             List<Statistiques[]> response = new ArrayList<Statistiques[]>();
             response.add(this.getStatCategoryGeneral(connexion));
             response.add(this.getStatTrajetGeneral(connexion));
//             Statistiques[][] response = new Statistiques[2][];
//             response[0] = this.getStatCategory(connexion);
//             response[1] = this.getStatTrajet(connexion);
             return response;
        }catch(Exception exp){
            throw exp;
        }
    }
}
