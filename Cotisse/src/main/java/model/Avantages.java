/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Connection.DBConnection;
import Generalise.FonctionReflect;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author mamai
 */
public class Avantages {
    String id;
    String avantages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvantages() {
        return avantages;
    }

    public void setAvantages(String avantages) {
        this.avantages = avantages;
    }

    public Avantages() {
    }

    public Avantages(String id, String avantages) {
        this.id = id;
        this.avantages = avantages;
    }
    
    public Avantages[] getAllAvantages(DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            List<Avantages> listeAvantages = (List<Avantages>) (Object) FonctionReflect.select("Avantages", new Avantages(), c.getConnection());
            Avantages[] resultat = new Avantages[listeAvantages.size()];
            resultat = listeAvantages.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection == true)
            {
                 c.clear();
            }
        }
    }
    
    public int deleteAv(String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null)
        {
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Avantages where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
    
            public void createA (Avantages avantages, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Avantages",avantages,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
}
