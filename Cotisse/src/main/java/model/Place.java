package model;
import Connection.DBConnection;
import java.sql.SQLException;
import java.sql.Statement;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Place {
    String id;
    String idVehicule;
    String numPlace;
    int etat;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVehicule() {
        return this.idVehicule;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getNumPlace() {
        return this.numPlace;
    }

    public void setNumPlace(String numPlace) {
        this.numPlace = numPlace;
    }
    
    public int getEtat(){
        return this.etat;
    }
    
    public void setEtat(int etat){
        this.etat = etat;
    }
    
    public Place(String id, String idVehicule, String numPlace, int etat) {
        this.id = id;
        this.idVehicule = idVehicule;
        this.numPlace = numPlace;
        this.etat = etat;
    }
      public Place() {
        
    }
     public int updatePlace(Place place, String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql = "update Place set id = '"+place.getId()+"', set idVehicule = '"+place.getIdVehicule()+"', set numPlace = '"+place.getNumPlace()+"', set etat = '"+place.getEtat()+"' where id ='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch (SQLException e){
            throw e;
        }
        finally{
            if(connection== true){
                c.clear();
            }
        }
    }
}