package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
import Fonction.Fonction;
import Generalise.FonctionReflect;
import java.sql.*;
import java.util.List;
import webServiceClass.reservationAffiche;
public class Utilisateurs {
    String id;
    String nom;
    String prenom;
    String sexe;
    Date date;
    String telephone;
    String email;
    String mdp;
    String idProfil;

    public Utilisateurs() {
            }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return this.sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return this.mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getIdProfil() {
        return this.idProfil;
    }

    public void setIdProfil(String idProfil) {
        this.idProfil = idProfil;
    }

    public Utilisateurs(String id, String nom, String prenom, String sexe, Date date, String telephone, String email, String mdp, String idProfil) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.date = date;
        this.telephone = telephone;
        this.email = email;
        this.mdp = mdp;
        this.idProfil = idProfil;
    }
    public int verifDoublon(Connection c) throws Exception{
        try{
            List<Object> objList = FonctionReflect.selectWithCondition("Utilisateurs",this, "WHERE email = '"+this.getEmail()+"'", c);
            if(objList.isEmpty()){
                return 0;
            }
            else{
                return 1;
            }
        }catch(Exception exp){
            throw exp;
        }
    }
    public Utilisateurs login(String username,String password,Connection c) throws Exception{
        try{
            Fonction f = new Fonction();
            String mdp = f.generateMdp(password, c);
            List<Object> objList = FonctionReflect.selectWithCondition("Utilisateurs", new Utilisateurs(), "WHERE email = '"+username+"' and motDePasse = '"+mdp +"'",c);
            if(objList.isEmpty()){
                throw new Exception("Oops! Utilisateurs introuvable.");
            }else{
                Utilisateurs response = (Utilisateurs) objList.get(0);
                return response;
            }
        }catch(Exception exp){
            throw exp;
        }
    }
    public Utilisateurs inscription(String nom,String prenom,String sexe,String dateDeNaissance,String telephone,String email,String motDePasse,Connection c) throws Exception{
        try{
            String id = FonctionReflect.generateId("idUtilisateurs", "USR-",c);
            Date dNaissance = Date.valueOf(dateDeNaissance);
            Utilisateurs insertObj = new Utilisateurs(id,nom,prenom,sexe,dNaissance,telephone,email,motDePasse,"PRF-1");
            int doublon = insertObj.verifDoublon(c);
            if(doublon == 1){
                throw new Exception("Cette adresse mail est déja utilisée sur ce site.");
            }
            else{
                try{
                     FonctionReflect.insert("Utilisateurs", insertObj, c);
                }catch(Exception exep){
                    throw exep;
                }
               return insertObj;
                
            }
        }catch(Exception exp){
            throw exp;
        }
        
    }
    
    public reservationAffiche[] mesReservations(String idUtilisateurs,Connection connection) throws Exception{
        try{
            reservationAffiche[] response = null ;
            reservationAffiche reference = new reservationAffiche();
            List<Object> listObj = FonctionReflect.selectWithCondition("reservationAffiche", reference, "where iutilisateurs ='"+ idUtilisateurs +"'", connection);
            return listObj.toArray(response);
        }catch(Exception exp){
            throw exp;
        }
    }
}
