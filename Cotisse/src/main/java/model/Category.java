package model;
import Generalise.FonctionReflect;
import Connection.DBConnection;
import java.sql.Statement;
import java.util.List;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */
public class Category {
    String id;
    String category;
    Double pourcentage;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPourcentage() {
        return this.pourcentage;
    }

    public void setPourcentage(Double pourcentage) {
        this.pourcentage = pourcentage;
    }

    public Category(String id, String category, Double pourcentage) {
        this.id = id;
        this.category = category;
        this.pourcentage = pourcentage;
    }
    public Category() {    
    }
     public void createC (Category category, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            FonctionReflect.insert("Categorie",category,c.getConnection());
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true  ){
                c.clear();
            }
        }
    }
    public int deleteC(String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c==null){
            c=new DBConnection();
            connection=true;
        }
        try{
            String sql = "delete from Categorie where id='"+id+"'";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection==true){
                c.clear();
            }
        }
    }
    
    public Category[] getAllCategorie(DBConnection c) throws Exception {
        boolean connection =false;
        if(c ==null){
            c =new DBConnection();
            connection=true;
        }
        try {
            List<Category> listeCategorie= (List<Category>) (Object) FonctionReflect.select("Categorie",new Category(), c.getConnection()) ;
            Category[] resultat = new Category[listeCategorie.size()];
            resultat=listeCategorie.toArray(resultat);
            return resultat;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            if(connection ==true){
                c.clear();
            }
        }
    }
}