package model;

import Generalise.FonctionReflect;
import Connection.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mamai
 */

public class BilletDetails {
    String id;
    String idReservation;
    String idBillet;
    Double prix;


    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdReservation() {
        return this.idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }

    public String getIdBillet() {
        return this.idBillet;
    }

    public void setIdBillet(String idBillet) {
        this.idBillet = idBillet;
    }

    public Double getPrix() {
        return this.prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public BilletDetails(String id, String idReservation, String idBillet, Double prix) {
        this.id = id;
        this.idReservation = idReservation;
        this.idBillet = idBillet;
        this.prix = prix;
    }
     public BilletDetails(){
        
    }
        public void insertDetails(Reservation reservation, String idPlace,Connection c) throws Exception{
        try{
            String idBD;
            idBD = FonctionReflect.generateId("idBilletDetails","BLLD-", c);
            Voyage v= reservation.getByid(reservation.getIdVoyage(), c);
            Trajet t= v.getByid(v.getIdTrajet(), c);
            Vehicule vehicl =v.getVehicule(v.getIdVehicule(), c);
            Category cat=vehicl.getCategorie(vehicl.getIdCategory(), c);
             Double prix=t.getPrix()+cat.getPourcentage();
            BilletDetails newBillet = new BilletDetails(idBD,reservation.getId(),this.getId(),prix);
            FonctionReflect.insert("Billet", newBillet, c);
            
        }catch(Exception exp){
            throw exp;
        }
    }
        public int updateBilletDetails(BilletDetails billetDetails, String id, DBConnection c) throws Exception{
        boolean connection = false;
        if(c == null){
            c = new DBConnection();
            connection = true;
        }
        try{
            String sql = "update BilletDetails set id= '"+billetDetails.getId()+"', set idReservation = '"+billetDetails.getIdReservation()+"', set idBillet = '"+billetDetails.getIdBillet()+"', set prix = "+billetDetails.getPrix()+"";
            Statement stmt = c.getConnection().createStatement();
            return stmt.executeUpdate(sql);
        }
        catch(SQLException e){
            throw e;
        }
        finally{
            if(connection == true){
                c.clear();
            }
        }
    }
}