/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webServiceClass;

import java.sql.Date;

/**
 *
 * @author andya
 */
public class reservationAffiche {
    String idVoyage;
    String immatriculation;
    String idTrajet;
    String depart;
    String arrivee;
    String heure;
    int place;
    String idCategory;
    String category;
    Double pourcentage;
    Float prix;
    Float prixTotal;
    String idReservation;
    String iutilisateurs;
    Date daty;
    int etatReservation;
    String idPlace;
    int numPlace;

    public String getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(Double pourcentage) {
        this.pourcentage = pourcentage;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Float getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Float prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }

    public String getIutilisateurs() {
        return iutilisateurs;
    }

    public void setIutilisateurs(String iutilisateurs) {
        this.iutilisateurs = iutilisateurs;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public int getEtatReservation() {
        return etatReservation;
    }

    public void setEtatReservation(int etatReservation) {
        this.etatReservation = etatReservation;
    }

    public String getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }

    public int getNumPlace() {
        return numPlace;
    }

    public void setNumPlace(int numPlace) {
        this.numPlace = numPlace;
    }

    public reservationAffiche() {
    }

    public reservationAffiche(String idVoyage, String immatriculation, String idTrajet, String depart, String arrivee, String heure, int place, String idCategory, String category, Double pourcentage, Float prix, Float prixTotal, String idReservation, String iutilisateurs, Date daty, int etatReservation, String idPlace, int numPlace) {
        this.idVoyage = idVoyage;
        this.immatriculation = immatriculation;
        this.idTrajet = idTrajet;
        this.depart = depart;
        this.arrivee = arrivee;
        this.heure = heure;
        this.place = place;
        this.idCategory = idCategory;
        this.category = category;
        this.pourcentage = pourcentage;
        this.prix = prix;
        this.prixTotal = prixTotal;
        this.idReservation = idReservation;
        this.iutilisateurs = iutilisateurs;
        this.daty = daty;
        this.etatReservation = etatReservation;
        this.idPlace = idPlace;
        this.numPlace = numPlace;
    }
    
}
