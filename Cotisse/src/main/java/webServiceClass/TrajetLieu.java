/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webServiceClass;

/**
 *
 * @author mamai
 */
public class TrajetLieu {
    String id;
    String heure;
    double prix;
    String lieu;
    String lieuarrivee;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getLieuarrivee() {
        return lieuarrivee;
    }

    public void setLieuarrivee(String lieuarrivee) {
        this.lieuarrivee = lieuarrivee;
    }

    public TrajetLieu(String id, String heure, double prix, String lieu, String lieuarrivee) {
        this.id = id;
        this.heure = heure;
        this.prix = prix;
        this.lieu = lieu;
        this.lieuarrivee = lieuarrivee;
    }

    public TrajetLieu() {
    }
}
