/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webServiceClass;

import Generalise.FonctionReflect;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author andya
 */
public class voyageAffiche {
    String idVoyage;
    String immatriculation;
    String idTrajet;
    String depart;
    String arrivee;
    String heure;
    int place;
    String idCategory;
    String category;
    Double pourcentage;
    Float prix;
    Float prixTotal;

    public String getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(Double pourcentage) {
        this.pourcentage = pourcentage;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Float getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Float prixTotal) {
        this.prixTotal = prixTotal;
    }

    public voyageAffiche(String idVoyage, String immatriculation, String idTrajet, String depart, String arrivee, String heure, int place, String idCategory, String category, Double pourcentage, Float prix, Float prixTotal) {
        this.idVoyage = idVoyage;
        this.immatriculation = immatriculation;
        this.idTrajet = idTrajet;
        this.depart = depart;
        this.arrivee = arrivee;
        this.heure = heure;
        this.place = place;
        this.idCategory = idCategory;
        this.category = category;
        this.pourcentage = pourcentage;
        this.prix = prix;
        this.prixTotal = prixTotal;
    }

    public voyageAffiche() {
    }
    
    public voyageAffiche[] getVoyage(Connection c) throws Exception{
        voyageAffiche[] resp = null;
        try{
            voyageAffiche reference = new voyageAffiche();
            List<Object> liste = FonctionReflect.select("voyageAffiche",reference, c);
            resp = liste.toArray(resp);
        }catch(Exception e){
            throw e;
        }
        return resp;
    }
}
