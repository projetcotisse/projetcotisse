/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webServiceClass;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author mamai
 */
public class Reserver {
    String id;
    String idVoyage;
    String idPlace;
    String iUtilisateurs;
    Timestamp daty;
    int etat;
    String idVehicule;
    String idTrajet;
    String numPlace;
    String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }

    public String getIUtilisateurs() {
        return iUtilisateurs;
    }

    public void setIUtilisateurs(String iUtilisateurs) {
        this.iUtilisateurs = iUtilisateurs;
    }

    public Timestamp getDaty() {
        return daty;
    }

    public void setDaty(Timestamp daty) {
        this.daty = daty;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getIdVehicule() {
        return idVehicule;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public String getNumPlace() {
        return numPlace;
    }

    public void setNumPlace(String numPlace) {
        this.numPlace = numPlace;
    }

    public Reserver(String id, String idVoyage, String idPlace, String iUtilisateurs, Timestamp daty, int etat, String idVehicule, String idTrajet, String numPlace, String nom) {
        this.id = id;
        this.idVoyage = idVoyage;
        this.idPlace = idPlace;
        this.iUtilisateurs = iUtilisateurs;
        this.daty = daty;
        this.etat = etat;
        this.idVehicule = idVehicule;
        this.idTrajet = idTrajet;
        this.numPlace = numPlace;
        this.nom = nom;
    }

    public Reserver() {
    }
    
}
