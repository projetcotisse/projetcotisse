/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webServiceClass;

/**
 *
 * @author mamai
 */
public class VehiculeCategorie {


    String immatriculation;
    int place;
    String idCategory;
    String category;
    Double pourcentage;

    
    public VehiculeCategorie(String immatriculation, int place, String idCategory, String category, Double pourcentage) {
        this.immatriculation = immatriculation;
        this.place = place;
        this.idCategory = idCategory;
        this.category = category;
        this.pourcentage = pourcentage;
    }
    public VehiculeCategorie() {
    }

    public String getImmatriculation() {
        return this.immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public int getPlace() {
        return this.place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPourcentage() {
        return this.pourcentage;
    }

    public void setPourcentage(Double pourcentage) {
        this.pourcentage = pourcentage;
    }
}