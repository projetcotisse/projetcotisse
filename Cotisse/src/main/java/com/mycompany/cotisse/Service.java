/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cotisse;

import Connection.DBConnection;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.*;
import webServiceClass.Inscription;
import webServiceClass.Login;
import webServiceClass.voyageAffiche;
import model.Vehicule;
import webServiceClass.VehiculeCategorie;
import model.Trajet;
import model.Avantages;
import webServiceClass.Reserver;
import webServiceClass.TrajetLieu;

/**
 * REST Web Service
 *
 * @author andya
 */
@Path("servicetest")

public class Service {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Service
     */
    public Service() {
    }

    @Path("/login")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(Login log) throws Exception{
        DBConnection c = new DBConnection();
        Utilisateurs u = new Utilisateurs();
        try{
             u = u.login(log.getUsername(),log.getPassword(),c.getConnection());
             return Response.ok().entity(u).build();
             
        }catch(Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }finally{
            c.clear();
        }
    }
    
    @Path("/inscription")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inscription(Inscription inscription) throws Exception{
        DBConnection c = new DBConnection();
        Utilisateurs u = new Utilisateurs();
        try{
           u = u.inscription(inscription.getNom(), inscription.getPrenom(), inscription.getSexe(), inscription.getDateDeNaissance(), inscription.getTelephone(), inscription.getEmail(), inscription.getMotDePasse(), c.getConnection());
            return Response.ok().entity(u).build();
        }catch(Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }finally{
            c.clear();
        }
        
    }
    /**
     * Retrieves representation of an instance of com.mycompany.cotisse.Service
     * @return an instance of java.lang.String
     */
    @Path("/test")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        return "STRING";
    }
    
    @Path("/listeVoyage")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVoyage() throws Exception {
       DBConnection c = new DBConnection();
        try{
            voyageAffiche voy = new voyageAffiche();
            voyageAffiche[] liste = voy.getVoyage(c.getConnection());
            return Response.ok().entity(liste).build();
        }catch(Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }finally{
            c.clear();
        }
    }
       
    @Path("/listeVehicule")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVehicule() throws Exception{
        DBConnection c = new DBConnection();
        try{
            Vehicule vehicule = new Vehicule();
            VehiculeCategorie[] liste = vehicule.getVehiclCat(c);
            return Response.ok().entity(new GenericEntity<VehiculeCategorie[]>(liste){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
        }
        catch (Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }
        finally{
            c.clear();
        }
    }
    
     @Path("/ajouterVehicules")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ajouterV(Vehicule vehicule) throws Exception{
        DBConnection c = new DBConnection();
        try{
            vehicule.createV(vehicule,c);
            VehiculeCategorie[] liste = vehicule.getVehiclCat(c);
            c.clear();
            return Response.status(200)
                    .entity(new GenericEntity<VehiculeCategorie[]>(liste){})
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .build();
        }
        catch (Exception e){
            return Response.status(404)
                    .entity(e.getMessage())
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
                    .allow("OPTIONS").build();
        }
    }
    
    @Path("/listeReservation")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReservation() throws Exception{
        DBConnection c = new DBConnection();
        try{
            Reservation reservation = new Reservation();
            Reserver[] liste = reservation.getAllReserver(c);
            return Response.ok().entity(new GenericEntity<Reserver[]>(liste) {})
             .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
        }
        catch(Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }
        finally{
            c.clear();
        }
    }
    
    @Path("/listeTrajet")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTrajet() throws Exception{
        DBConnection c = new DBConnection();
        try{
            Trajet trajet = new Trajet();
            TrajetLieu[] liste = trajet.getTrajetLieu(c);
            return Response.ok().entity(new GenericEntity<TrajetLieu[]>(liste){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
        }
        catch (Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }
        finally{
            c.clear();
        }
    }
    
         @Path("/ajouterTrajet")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ajouterT(Trajet trajet) throws Exception{
        DBConnection c = new DBConnection();
        try{
            trajet.createT(trajet,c);
            TrajetLieu[] liste = trajet.getTrajetLieu(c);
            c.clear();
            return Response.status(200)
                    .entity(new GenericEntity<TrajetLieu[]>(liste){})
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .build();
        }
        catch (Exception e){
            return Response.status(404)
                    .entity(e.getMessage())
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
                    .allow("OPTIONS").build();
        }
    }
    
    @Path("/supprimerTrajet/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void supprimer(@PathParam("id")String id) throws Exception{
        DBConnection c = new DBConnection();
        try{
            Trajet trajet = new Trajet();
            trajet.deleteT(id,c);                                                                                                                                                                                                                                                                                                                                                                       
            /*TrajetLieu[] liste1 = trajet.getTrajetLieu(c);
            return Response.ok().entity(new GenericEntity<TrajetLieu[]>(liste1){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
            */       
        }
        catch (Exception exp) {
            return ;
        }
        finally{
            c.clear();
        }
    }
    
        @Path("/supprimerReservation/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void supprimerRes(@PathParam("id")String id) throws Exception{
        DBConnection c = new DBConnection();
        try{
            Reservation reservation = new Reservation();
            reservation.deleteRes(id,c);                                                                                                                                                                                                                                                                                                                                                                       
            /*TrajetLieu[] liste1 = trajet.getTrajetLieu(c);
            return Response.ok().entity(new GenericEntity<TrajetLieu[]>(liste1){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
            */       
        }
        catch (Exception exp) {
            return ;
        }
        finally{
            c.clear();
        }
    }
    
    @Path("/supprimerVehicule/{immatriculation}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void supprimerV(@PathParam("immatriculation")String immatriculation) throws Exception{
        DBConnection c = new DBConnection();
        try{
            Vehicule vehicule = new Vehicule();
            vehicule.deleteV(immatriculation,c);   
        }
        catch (Exception exp) {
            return ;
        }
        finally{
            c.clear();
        }
    }
        @Path("/listeAvantages")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvantages() throws Exception{
        DBConnection c = new DBConnection();
        try{
            Avantages avantages = new Avantages();
            Avantages[] liste = avantages.getAllAvantages(c);
            return Response.ok().entity(new GenericEntity<Avantages[]>(liste){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
        }
        catch (Exception exp){
            return Response.ok().entity(exp.getMessage()).build();
        }
        finally{
            c.clear();
        }
    }

    @Path("/supprimerAvantages/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void supprimerA(@PathParam("id")String id) throws Exception{
        DBConnection c = new DBConnection();
        try{
            Avantages avantages = new Avantages();
            avantages.deleteAv(id,c);                                                                                                                                                                                                                                                                                                                                                                       
            /*TrajetLieu[] liste1 = trajet.getTrajetLieu(c);
            return Response.ok().entity(new GenericEntity<TrajetLieu[]>(liste1){} )
            .header("Acces-Control-Allow-Origin","*")
            .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
            .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
            .allow("OPTIONS").build();
            */       
        }
        catch (Exception exp) {
            return ;
        }
        finally{
            c.clear();
        }
    }
    
    @Path("/ajouterAvantages")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ajouterA(Avantages avantage) throws Exception{
        DBConnection c = new DBConnection();
        try{
            avantage.createA(avantage,c);
            Avantages[] liste = avantage.getAllAvantages(c);
            c.clear();
            return Response.status(200)
                    .entity(new GenericEntity<Avantages[]>(liste){})
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
                    .build();
        }
        catch (Exception e){
            return Response.status(404)
                    .entity(e.getMessage())
                    .header("Acces-Control-Allow-Origin","*")
                    .header("Acces-Control-Allow-Methods","GET, POST, DELETE, PUT, OPTIONS, HEAD")
                    .header("Acces-Control-Allow-Headers", "Origin, X-Requested-With,Authorization, Content-Type,Accept")
                    .allow("OPTIONS").build();
        }
    }
    
    @Path("/hello")
    @GET
    public String hello(){
        return "hellooooooooooooooooooooo";
    }
    /**
     * PUT method for updating or creating an instance of Service
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
